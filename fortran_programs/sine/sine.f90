program sine
  implicit none
  !variable declaration
  integer :: n, i1
  real(kind=8), allocatable, dimension(:) :: f


  !read n
  open(unit=9,file='data.in') !read from file given and
  !assigned label 9
  read(9,*) n !read from label 9 and * means ANY variable type
  close(9)

  !see if reading n correctly
  print *, 'n = ',n

  allocate(f(n))

  !main code, calculations
  call calculations(n,f)
  print *, 'sine of first n integers', f(1:n)

end program sine

subroutine calculations(n,f)
implicit none
integer :: i1
integer, intent(in) :: n
real(kind=8), dimension(n), intent(out) :: f

  do i1 = 1,n
    f(i1) = sin(dble(i1))
  end do

end subroutine calculations
